/*
 * LineSensor.h
 *
 *  Created on: 27 mag 2018
 *      Author: Ilaria
 */

#ifndef LINE_SENSOR_H_
#define LINE_SENSOR_H_

#include "stdint.h"
#include "stm32f4xx_hal.h"
typedef struct
{
	GPIO_TypeDef* GPIO_port; /*!< GPIO port		If the pin is: PA13 then the port is GPIOA*/
	uint16_t GPIO_pin;       /*!< GPIO pin configured in input
   	   	   	   	   	   	   	   	   	   	   	   	If the pin is: PA13 then the pin is GPIO_PIN_13*/
}LineSensor;


/*
 * Prototype of functions used by the Rover
 * to use the sensors of line detection.
 */


/*
 * How many sensors are you including in your rover?
 */

void init_line_sensors(uint8_t n);

/*
 * Functions for debugging
 */

uint8_t log_line_sensor_string(char * log_msg);
void initialize_line_sensor_logger(UART_HandleTypeDef * huart);


/*
 * Get the result value of a single Sensor,
 * the one chosen by the numberSensor.
 * Available from 1 to 3 sensors.
 */

uint8_t getSensorLine(GPIO_TypeDef* port, uint16_t pin);

/*
 * Get the result value of all 3-Sensors
 * on a 8 bit value, mapped as follows:
 *
 * [ pin3 | pin2 | pin1 ]
 * 	  |		 |		|
 * [  x   |  x   |  x   ]
 *
 * If all sensors catch a line the result will be:
 * 		* 111 -> 7
 * If none catches a line the result is:
 * 		* 000 -> 0
 * If only 1-st sensor matches a line:
 * 		* 001 -> 1
 * If only 2-nd sensor matches a line:
 * 		* 010 -> 2
 * If only 3-rd sensor matches a line:
 * 		* 100 -> 4
 * If only [1-2] sensors match a line:
 * 		* 011 -> 3
 * If only [2-3] sensors match a line:
 * 		* 110 -> 6
 * If only [1-3] sensors match a line:
 * 		* 101 -> 5
 */

uint8_t get_threeSensorsLine(GPIO_TypeDef* port, uint16_t pin1, uint16_t pin2, uint16_t pin3);

/*
 * Get the result value of sensors from
 * x to y, e.g. from 2 to 3 will get the
 * second and the third sensor like this:
 * 		* 11 -> 3	both get a line
 * 		* 01 -> 1	only first catch a line
 * 		* 10 -> 2	only second catch a line
 * 		* 00 -> 0	no sensor match
 */

uint8_t get_twoSensorsLine(GPIO_TypeDef* port, uint16_t pin1, uint16_t pin2);

/*
 * Concatenate single bits into a string of bits
 */
uint8_t concatenate_two_bits(uint8_t x, uint8_t y);
uint8_t concatenate_three_bits(uint8_t x, uint8_t y, uint8_t z);

#endif /* LINE_SENSOR_H_ */
