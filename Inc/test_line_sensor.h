/*
 * test_line_sensor.h
 *
 *  Created on: 03 giu 2018
 *      Author: Ilaria
 */

#ifndef TEST_LINE_SENSOR_H_
#define TEST_LINE_SENSOR_H_

/* USER CODE BEGIN Includes */
#include "stm32f4xx_hal.h"
/* USER CODE END Includes */

/*
 * Variables
 */


/*
 * Prototype test functions
 */
void init_test_line_sensor(UART_HandleTypeDef * huart);
void test_3_bits_sensor_line_detection(GPIO_TypeDef* port, uint16_t pin1, uint16_t pin2, uint16_t pin3);
void test_2_bits_sensor_line_detection(GPIO_TypeDef* port, uint16_t pin1, uint16_t pin2);
void test_1_bit_sensor_line_detection(GPIO_TypeDef* port, uint16_t pin);

#endif /* TEST_LINE_SENSOR_H_ */
