# Pololu QTR-1RC reflectance sensor
From the official site of the product [https://www.pololu.com/product/958], supplied 5V voltage, this library allows to connect 1-to-3 sensors to the board which grant line detection.

In the library, functions available manage to retrieve the results from:
- only one sensor
- a pair of sensors
- all sensors

In the basic case of 1 sensor usage, the library returns:
- 1 if a line is caught,
- 0 otherwise.

Let's extend this to 3 sensors. Each sensor returns a bit, but the library only returns a 8 bit value. The sensors are mapped as follows:

 | pin3 | pin2 | pin1 |
 |---|---|---|
 |  x   |  x   |  x   |

in the hypotesis that the pin's 1-to-3 scheme follows the order you connect the sensors to the main source code and to the whole system.


 If all sensors catch a line the result will be:
```
111 -> 7 
```
If none catches a line the result is:
```
000 -> 0
```
If only 1-st sensor matches a line:
```
001 -> 1
```
If only 2-nd sensor matches a line:
```
010 -> 2
```
If only 3-rd sensor matches a line:
```
100 -> 4
```
If only [1-2] sensors match a line:
```
011 -> 3
```
If only [2-3] sensors match a line:
```
110 -> 6
```
If only [1-3] sensors match a line:
```
101 -> 5
```

