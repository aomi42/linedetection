/*
 * test_line_sensor.c
 *
 *  Created on: 03 giu 2018
 *      Author: Ilaria
 */
/* USER CODE BEGIN Includes */
#include <test_line_sensor.h>
#include "line_sensor.h"
/* USER CODE END Includes */


void init_test_line_sensor(UART_HandleTypeDef * huart){
	initialize_line_sensor_logger(huart);
	init_line_sensors(3);
}

void test_3_bits_sensor_line_detection(GPIO_TypeDef* port, uint16_t pin1, uint16_t pin2, uint16_t pin3){
	uint8_t i;
	for (i=0; i<16; i++){
		uint8_t v = get_threeSensorsLine(port, pin1, pin2, pin3);
		log_line_sensor_string("Reading");
		char str[5] = ".";
		log_line_sensor_string(str);
		HAL_Delay(200);
		log_line_sensor_string(str);
		HAL_Delay(200);
		log_line_sensor_string(str);
		HAL_Delay(200);

		char str_buffer[5];
		sprintf(str_buffer, " 3-bits value = %d\r\n", v);
		log_line_sensor_string(str_buffer);
		HAL_Delay(500);
	}

}
void test_2_bits_sensor_line_detection(GPIO_TypeDef* port, uint16_t pin1, uint16_t pin2){
	uint8_t i;
	for (i=0; i<16; i++){
		uint8_t v = get_twoSensorsLine(port, pin1, pin2);
		log_line_sensor_string("Reading");
		char str[5] = ".";
		log_line_sensor_string(str);
		HAL_Delay(200);
		log_line_sensor_string(str);
		HAL_Delay(200);
		log_line_sensor_string(str);
		HAL_Delay(200);

		char str_buffer[5];
		sprintf(str_buffer, " 2-bits value = %d\r\n", v);
		log_line_sensor_string(str_buffer);
		HAL_Delay(500);
	}
}

void test_1_bit_sensor_line_detection(GPIO_TypeDef* port, uint16_t pin){
	uint8_t i;
	for (i=0; i<16; i++){
		uint8_t v = getSensorLine(port, pin);
		log_line_sensor_string("Reading");
		char str[5] = ".";
		log_line_sensor_string(str);
		HAL_Delay(200);
		log_line_sensor_string(str);
		HAL_Delay(200);
		log_line_sensor_string(str);
		HAL_Delay(200);

		char str_buffer[5];
		sprintf(str_buffer, " 1-bit value = %d\r\n", v);
		log_line_sensor_string(str_buffer);
		HAL_Delay(500);
	}
}


