/*
 * LineSensor.c
 *
 *  Created on: 27 mag 2018
 *      Author: Ilaria
 */
/* USER CODE BEGIN Includes */

#include "line_sensor.h"
#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <string.h>

/* USER CODE END Includes */

UART_HandleTypeDef* line_uart;
uint8_t number_of_sensors;
/* Variables ---------------------------------------------------------*/


void init_line_sensors(uint8_t n)
{
	number_of_sensors = n;
}

uint8_t concatenate_two_bits(uint8_t x, uint8_t y)
{
	/*
	uint8_t *arr = (uint8_t *) malloc(2*sizeof(uint8_t));
	arr[0] = !(x);
	arr[1] = !(y);
	return arr;*/

	return  y << 1 | x;
}

uint8_t concatenate_three_bits(uint8_t x, uint8_t y, uint8_t z)
{
	/*uint8_t *arr = (uint8_t *) malloc(3*sizeof(uint8_t));
	arr[0] = !(x);
	arr[1] = !(y);
	arr[2] = !(z);
	return arr;
	free(arr);*/
	return  z << 2 | y << 1 | x;
}

uint8_t getSensorLine(GPIO_TypeDef* port, uint16_t pin)
{
	/*
	switch(numberSensor){
	case(1):
			return !((uint8_t)HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_13));
	case(2):
			return !((uint8_t)HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_14));
	case(3):
			return !((uint8_t)HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_15));
	default:
			return !((uint8_t)HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_13));
	}
	*/

	return !((uint8_t) HAL_GPIO_ReadPin(port, pin) );
	//return !( (uint8_t) HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_13) );
}


uint8_t get_threeSensorsLine(GPIO_TypeDef* port, uint16_t pin1, uint16_t pin2, uint16_t pin3)
{
	return concatenate_three_bits(getSensorLine(port,pin1),
					getSensorLine(port,pin2),
					getSensorLine(port,pin3));
}


uint8_t get_twoSensorsLine(GPIO_TypeDef* port, uint16_t pin1, uint16_t pin2)
{
	return concatenate_two_bits(getSensorLine(port,pin1),getSensorLine(port,pin2));
}

void initialize_line_sensor_logger(UART_HandleTypeDef* huart)
{
	line_uart = huart;
	log_line_sensor_string("Ready to transmit.\r\n");
}

uint8_t log_line_sensor_string(char * log_msg)
{
	if(HAL_UART_Transmit(line_uart, (uint8_t *)log_msg, strlen(log_msg), 0xFFFF) == HAL_OK)
		return 1;
	else
		return 0;
}


